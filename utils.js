/*jslint node: true */
'use strict';

var treeLib = require('./tree');
var csv = require('fast-csv');
var treeLib = require('./tree');


function searchForNode(withName, node) {
  if(node.name() == withName) {
    return node;
  }
  else {
    var children = node.children();

    for(var i=0; i < children.length; i++) {
      var result = searchForNode(withName, children[i]);
      if(result !== null) {
        return result;
      }
    }
  }
  return null;
}

function searchChildrenFor(pairs, search_for_node) {
  var children = [];

  for(var i=0; i<pairs.length; i++) {
    if(pairs[i].parent == search_for_node.name()) {
      var childNode = treeLib.createTreeNode(pairs[i].name);
      childNode.addChildren(searchChildrenFor(pairs, childNode));
      children.push(childNode);
    }
  }
  return children;
}

function readTreeNodeCSV(csvFilepath, callback) {
  var pairs = [];
  csv.fromPath(csvFilepath, { 'headers': true, 'trim': true })
    .on('data', function(data){
      pairs.push(data);
    })
    .on('end', function(){
      var rootNode = treeLib.createTreeNode('root');
      var rootChildNodes = searchChildrenFor(pairs, rootNode);
      rootNode.addChildren(rootChildNodes);

      callback(rootNode);
    });
}

function printTree(treenode, level) {
  if(typeof level === 'undefined') {
    level = 0;
  }

  function dashes(n) {
    if(n <= 0) return '';
    n--;
    var s = '';
    for(var i=0; i<n; i++) {
      s = s + '  ';
    }
    return s + '+ ';
  }

  console.log(dashes(level) + treenode.name());
  for(var i=0; i<treenode.children().length; i++) {
    printTree(treenode.children()[i], level + 1);
  }
}

module.exports = {
  searchChildrenFor: searchChildrenFor,
  printTree: printTree,
  readTreeNodeCSV: readTreeNodeCSV,
  searchForNode: searchForNode
};
