var hippie = require('hippie');
var serverApp = require('../server');

describe('All fruits', function () {
  describe('end-to-end', function () {
    it('should return all fruits in a simple list', function(done) {
      hippie(serverApp)
      .json()
      .get('/all-fruits')
      .expectStatus(200)
      .expectValue('.length', 2)
      .expectValue('[0]', 'sour')
      .expectValue('[1]', 'sweet')
      .end(function(err, res, body) {
          if (err) done(err);
          else done();
      });
    });
  });
});
