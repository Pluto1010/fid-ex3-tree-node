var hippie = require('hippie');
var serverApp = require('../server');

describe('Exactly two children', function() {
  describe('end-to-end', function() {
    it('should return all node names with exactly two children', function(done) {
      hippie(serverApp)
      .json()
      .get('/with-two-children')
      .expectStatus(200)
      .expectValue('.length', 5)
      .expectValue('[0]', 'cheese')
      .expectValue('[1]', 'fruit')
      .expectValue('[2]', 'root')
      .expectValue('[3]', 'sour')
      .expectValue('[4]', 'yellow')
      .end(function(err, res, body) {
          if (err) done(err);
          else done();
      });
    });
  });
});
