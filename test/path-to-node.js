var hippie = require('hippie');
var serverApp = require('../server');

describe('Path to node', function() {
  describe('end-to-end', function() {
    it('should the complete path to lemon', function(done) {
      hippie(serverApp)
      .json()
      .get('/pathto/lemon')
      .expectStatus(200)
      .expectBody('"/fruit/sour/citrus/lemon"')
      .end(function(err, res, body) {
          if (err) done(err);
          else done();
      });
    });
    it('should the complete path to parmesan', function(done) {
      hippie(serverApp)
      .json()
      .get('/pathto/parmesan')
      .expectStatus(200)
      .expectBody('"/cheese/yellow/parmesan"')
      .end(function(err, res, body) {
          if (err) done(err);
          else done();
      });
    });
    it('should return 404 when item unknown', function(done) {
      hippie(serverApp)
      .json()
      .get('/pathto/foo')
      .expectStatus(404)
      .end(function(err, res, body) {
          if (err) done(err);
          else done();
      });
    });
    it('should the complete path to root', function(done) {
      hippie(serverApp)
      .json()
      .get('/pathto/root')
      .expectStatus(200)
      .expectBody('"/"')
      .end(function(err, res, body) {
          if (err) done(err);
          else done();
      });
    });
  });
});
