# Execise Task #3 - Tree Node

Developed using Node JS 5.8.0

## Run:
- ```git clone https://bitbucket.org/Pluto1010/fid-ex3-tree-node.git```
- ```cd fid-ex3-tree-node```
- ```npm install```
- ```npm test```
- ```npm start```

## HTTP endpoints on http://localhost:8080

- /pathto/:name
- /with-two-children
- /all-fruits

Parameter :name can be anythings of the elements in treenodes.csv files. i.e. lemon

## Inside Docker container

Assuming you have Docker installed run:

- ```docker build -t fid-ex3-treenode .```
- ```docker run -p 8080:8080 fid-ex3-treenode```
