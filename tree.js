/*jslint node: true */
'use strict';
var TreeNode = function(name) {
  var my_children = [];
  var parent = null;
  return {
    name: function() {
      return name;
    },
    children: function() {
      return my_children;
    },
    parent: parent,
    addChildren: function(treenodes) {
      for(var i = 0; i < treenodes.length; i++) {
        treenodes[i].parent = this;
      }

      my_children = my_children.concat(treenodes);
    }
  };
};

module.exports = {
  createTreeNode: function(name) {
    return new TreeNode(name);
  }
};
