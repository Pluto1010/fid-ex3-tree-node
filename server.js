/*jslint node: true */
'use strict';
var restify = require('restify');
var utils = require('./utils');
var workingTree;


function completePathForNode(req, res, next) {
  function completePath(targetNode) {
    var pathElements = [];
    var node = targetNode;

    do {
      if(node.parent !== null) {
        pathElements.push(node);
      }

      node = node.parent;
    } while (node !== null);

    return '/' + pathElements.reverse().map((x) => x.name()).join('/');
  }

  var node = utils.searchForNode(req.params.name, workingTree);
  if(node === null) {
    res.send(404);
    next();
    return;
  }

  var path = completePath(node);
  res.send(path);
  next();
}

function withTwoChildren(req, res, next) {
  var resultNodes = [];
  function searchForNodesWithTwoChildren(node) {
    var children = node.children();
    if(children.length == 2) {
      resultNodes.push(node);
    }

    for(var i=0; i < children.length; i++) {
      searchForNodesWithTwoChildren(children[i]);
    }
  }

  searchForNodesWithTwoChildren(workingTree);
  res.send(resultNodes.map((x) => x.name()).sort());
  next();
}

function allFruits(req, res, next) {
  var fruits = utils.searchForNode('fruit', workingTree);
  res.send(fruits.children().map((x) => x.name()));
  next();
}

var server = restify.createServer();

function initialize_server() {
  server.name = 'Exercise Task #3 - Tree Node';
  server.get('/pathto/:name', completePathForNode);
  server.get('/with-two-children', withTwoChildren);
  server.get('/all-fruits', allFruits);
}

utils.readTreeNodeCSV('treenodes.csv', function(treeRootNode) {
  //utils.printTree(treeRootNode);
  workingTree = treeRootNode;
  initialize_server();
});

module.exports = server;
